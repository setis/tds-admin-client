$.ajaxSetup({
    type: "GET",
    xhrFields: {withCredentials: true},
    crossDomain:true
});
var
        func = require('func'),
        auth = require('auth'),
        domain = require('domain'),
        server = require('server'),
        user = require('user');
auth.force();
window.closed = func.closed;
window.m = func.modal_switch;
$(document).ready(function () {
    auth.init();
    user.init();
    domain.init();
    server.init();
    
});

