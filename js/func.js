var configure = require('cfg');
function alert(id, type, message) {
    var cfg = configure.cfg.alert;
    if (cfg.eventShow.indexOf(type) === -1) {
        return;
    }
    id.html('<div class="alert ' + type + '"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message + '</div>');
    var alert = id.find('.alert');
    alert.alert();
    if (cfg.timeClose !== undefined) {
        setTimeout(function () {
            alert.alert('close');
        }, cfg.timeClose);
    }

}
function closed(e, block, select) {
    if (block === true) {
        var html = $(e).parent().find('.option-close').html();
        var id = $(e).attr('data-close');
        $(id).append(html);
    }
    if (select === true) {
        $(id).selectpicker('refresh');
    }
    $(e).parent().remove();
}
function options(data, fields, _use, exclusion) {
    var html = '', i, name, use, option;
    for (i in data) {
        option = data[i];
        if (exclusion === undefined || exclusion.indexOf(option[fields.value]) === -1) {
            use = (_use === option[fields.value]) ? 'selected' : '';
            html += '<option value="' + option[fields.value] + '"' + use + '>' + option[fields.name] + '</option>';
        }
    }
    return html;
}
function refresh(params) {
    if (params.cfg !== undefined && params.cfg.indexOf(params.event) !== -1) {
        params.dom.bootstrapTable('refresh');
    } else {
        if (typeof params.db === 'function') {
            params.db();
        }
        switch (params.event) {
            case 'remove':
                params.dom.bootstrapTable('remove', {
                    field: 'id',
                    values: params.data
                });
                break;
            case 'add':
                params.dom.bootstrapTable('append', [params.data]);
                break;
            case 'edit':
                params.dom.bootstrapTable('updateRow', {
                    index: params.index,
                    row: params.data
                });
                break;
        }
    }
}
function clear(form, params) {
    for (var i in params) {
        var name = params[i];
        form.find('[name="' + name + '"]').val('');
    }
}
function form(form, params) {
    for (var name in params) {
        var value = params['name'];
        form.find('[name="' + name + '"]').val(value);
    }
}
function modal_switch(close, open) {
    $(close).modal('hide');
    $(open).modal('show');
    $(open).find('[data-dismiss="modal"]').one('click', function () {
        $(close).modal('show');
    });
}
function gallery_img(id, id_links, values, gallery, imgLinks, func) {
    var img = configure.img;
    var html = '<div><div id="' + id + '" class="carousel slide" data-ride="carousel"><ol class="carousel-indicators hide">';
    for (var i in Object.keys(values)) {
        if (i === 0) {
            html += '<li data-target="#' + id + '" data-slide-to="' + i + '" class="active">' + i + '</li>';
        } else {
            html += '<li data-target="#' + id + '" data-slide-to="' + i + '">' + i + '</li>';
        }
    }
    var size = ++i;
    html += '</ol><div class="carousel-inner" role="listbox">';
    var f = true;
    if (imgLinks === undefined) {
        imgLinks = [];
    }
    if (gallery === undefined) {
        gallery = '';
    }
    var wrapper = document.createElement('div');
    wrapper.id = id + '-m';
    for (var name in values) {
        var arr = values[name];
        var alt = (arr['original'].alt === null) ? '' : arr['original'].alt;
        if (f === false) {
            html += '<div class="item"><img src="' + img(arr['200x200'].url)[0] + '" alt="' + alt + '" class="img-responsive center-block"></div>';
        } else {
            f = false;
            html += '<div class="item active"><img src="' + img(arr['200x200'].url)[0] + '" alt="' + alt + '" class="img-responsive center-block"></div>';
        }
        $('<a>').append($('<img>').prop('src', img(arr['200x200'].url)[0]))
                .prop('href', img(arr['original'].url)[0])
                .prop('title', alt)
                .attr('data-gallery', gallery)
                .appendTo(wrapper);
        imgLinks.push({
            href: img(arr['75x75'].url)[0],
            title: alt
        });
    }
    $(id_links).append(wrapper);
    html += '</div><form class="form-inline"><div class="form-group"><div class="input-group"><input type="button" class="btn btn-info" value="‹" onclick="$(\'#' + id + '\').carousel(\'prev\');"><div class="input-group-addon slider-number">1/' + size + '</div><input type="button" class="btn btn-info" value="›" onclick="$(\'#' + id + '\').carousel(\'next\');"></div></div></form></div></div>';
    setTimeout(function () {
        var target = $('#' + id);
        target.carousel('pause').on('slid.bs.carousel', function (e) {
            $('div.slider-number').text(($(this).find('li.active').data('slide-to') + 1) + '/' + size);
        });
        func(target, imgLinks);
    }, 750);
    return html;
}
module.exports = {
    alert: alert,
    closed: closed,
    clear: clear,
    form: form,
    options: options,
    refresh: refresh,
    modal_switch: modal_switch,
    gallery_img: gallery_img
};