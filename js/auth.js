var alert = require('func').alert,
        cfg = require('cfg'),
        dom = {},
        user = {},
        status = false;

function init() {
    dom = {
        modal: $(document.getElementById('modal-login')),
        form: $(document.getElementById('form-login')),
        alert: $(document.getElementById('alert-login')),
        action: $(document.getElementById('action-login'))
    };
    dom.modal.modal({
        keyboard: false,
        backdrop: false,
        show: true
    });
    dom.action.click(login);
    if (status === false) {
        is();
    }else{
        dom.modal.modal('hide');
    }
}
function id() {
    $.ajax({
        url: cfg.url('authorization.id'),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            user = data;
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'нет связи с сервером');
            status = false;
            dom.modal.modal('show');
        }
    });
}
function force() {
    $.ajax({
        url: cfg.url('authorization.is'),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            if (data === true) {
                status = true;
                id();
            } else {
                status = false;
            }
        },
        error: function (e) {
            status = false;
        }
    });
}
function is() {
    $.ajax({
        url: cfg.url('authorization.is'),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            if (data === true) {
                status = true;
                dom.modal.modal('hide');
                id();
            } else {
                alert(dom.alert, "alert-danger", 'ошибка пароля или логина');
                status = false;
                dom.modal.modal('show');
            }
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'нет связи с сервером');
            status = false;
            dom.modal.modal('show');
        }
    });
}
function login() {
    $.ajax({
        url: cfg.url('authorization.login'),
        dataType: 'json',
        data: dom.form.serializeArray(),
        method: 'post',
        success: function () {
            status = true;
            dom.modal.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'ошибка пароля или логина');
            status = false;
            dom.modal.modal('show');
        }
    });
    return false;
}
function logout() {
    $.ajax({
        url: cfg.url('authorization.logout'),
        dataType: 'json',
        method: 'get',
        success: function () {
            user = {};
            status = false;
            location.reload(true);
        },
        error: function () {
            location.reload(true);
            console.error(arguments);
        }
    });
    return false;
}
module.exports = {
    init: init,
    is: is,
    force: force,
    login: login,
    logout: logout,
    dom: dom,
    status: status,
    user: user
};