var cfg = {
    server: {
        api: 'http://localhost:8081/api/manga/',
        img: {
            upload: 'http://localhost:8081/api/manga/upload/img/',
            download: 'http://localhost:8081/img/'
        }

    },
    alert: {
        timeClose: 5 * 1e3,
        eventShow: ['alert-warning', 'alert-danger']
//        eventShow:['alert-success','alert-info','alert-warning','alert-danger']
    },
    weight: {
        img: [
            'fs'
        ]
    }

};
function uploadImg(action, params) {
    var url = cfg.server.img.upload + '?action=' + action;
    if (params !== undefined) {
        if (typeof params === 'object' && Object.keys(params).length !== 0) {
            var i;
            if (params instanceof Array) {
                for (i in params) {
                    url += '&in[]=' + params[i];
                }
            } else {
                for (i in params) {
                    url += '&in[' + i + ']=' + params[i];
                }
            }
        } else {
            url += '&in[]=' + params;
        }
    }
    return url;
}
function url2(action, params) {
    var url = cfg.server.api + '?action=' + action;
    if (params !== undefined) {
        var i;
        for (i in params) {
            var obj = params[i];
            url += obj.name + '=' + obj.value;
        }
    }
    return url;
}
function url(action, params, table) {
    var url = cfg.server.api + '?action=' + action;
    if (params !== undefined) {
        if (typeof params === 'object' && Object.keys(params).length !== 0) {
            var i;
            if (table === true) {
                for (i in params) {
                    var obj = params[i];
                    url += '&in[' + obj.name + ']=' + obj.value;
                }
            } else if (params instanceof Array) {
                for (i in params) {
                    url += '&in[]=' + params[i];
                }
            } else {
                for (i in params) {
                    url += '&in[' + i + ']=' + params[i];
                }
            }
        } else {
            url += '&in[]=' + params;
        }
    }
    return url;
}
function img(urls, weight) {
    var i, type, weightList = cfg.weight.img;
    if (weight === undefined) {
        for (i in weightList) {
            type = weightList[i];
            if (urls[type] !== undefined) {
                if (type === 'fs') {
                    return [cfg.server.img.download + urls[type], type];
                } else {
                    return [urls[type], type];
                }


            }
        }
    } else {
        var f = false;
        for (i in weightList) {
            type = weightList[i];
            if (f === false && weight !== type) {
                f = true;
            } else if (f === true && urls[type] !== undefined) {
                if (type === 'fs') {
                    return [cfg.server.img.download + urls[type], type];
                } else {
                    return [urls[type], type];
                }
            }
        }
    }
}
function params(params, table) {
    var result = {};
    if (params !== undefined) {
        if (typeof params === 'object' && Object.keys(params).length !== 0) {
            var i;
            if (table === true) {
                for (i in params) {
                    var obj = params[i];
                    result[obj.name] = obj.value;
                }
            } else {
                for (i in params) {
                    result[i] = params[i];
                    url += '&in[' + i + ']=' + params[i];
                }
            }
        } else {
            result = [params];
        }
    }
    return result;
}

module.exports = {
    cfg: cfg,
    url: url,
    uploadImg: uploadImg,
    img: img,
    params: params
};