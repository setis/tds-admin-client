var func = require('func'),
        alert = func.alert,
        cfg = require('cfg'),
        action_index,
        dom = {},
        configure = {},
        db = {
            index: {},
            data: {}
        },
//user = require('auth').user,
setting = {
    refreshTable: undefined
};
function refresh(event, data) {
    func.refresh({
        cfg: setting.refreshTable,
        event: event,
        data: data,
        db: function () {
            db = {
                index: {},
                data: {}
            };
        },
        index: action_index,
        dom: dom.table
    });
}
function add() {
    var db = dom.form.add.serializeArray();
    $.ajax({
        url: cfg.url('user.create', db, true),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            alert(dom.alert, 'alert-success', "пользователь создан");
            refresh('add', data);
            dom.modal.add.modal('hide');
            dom.form.add.find('input[type="text"]').val('');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно создать пользователя');
            console.error(e);
        }
    });
    return false;
}
function edit() {
    var db = dom.form.edit.serializeArray();
    $.ajax({
        url: cfg.url('user.change', db, true),
        dataType: 'json',
        method: 'get',
        success: function (data) {
            alert(dom.alert, 'alert-success', "измененно язык");
            refresh('edit', data);
            dom.modal.edit.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно добавлен язык');
            dom.modal.edit.modal('hide');
            console.error(e);
        }
    });
    return false;
}
function remove() {
    var id = dom.form.remove.find('input[name="id"]').val();
    $.ajax({
        url: cfg.url('user.remove'),
        data: {in: {id: id}},
        dataType: 'json',
        method: 'get',
        success: function () {
            alert(dom.alert, 'alert-success', "удаленно язык");
            refresh('remove', [parseInt(id)]);
            dom.modal.remove.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно добавлен язык');
            dom.modal.remove.modal('hide');
            console.error(e);
        }
    });
    return false;
}
function search(event, text) {
    if (text.length === 0) {
        return;
    }
    $.ajax({
        url: cfg.url('lang.search', {'search': text}),
        dataType: 'json',
        method: 'GET',
        cache: false,
        success: function (data) {
            if (data.error === undefined) {
                alert(dom.alert, 'alert-success', "успешно выполнено языки");
                console.info(text, data);
                dom.table.bootstrapTable('load', {
                    data: data
                });
//                dom.table.bootstrapTable('append', data);
                dom.table.bootstrapTable('scrollTo', 'bottom');
            } else {
                alert(dom.alert, 'alert-warning', data.error);
                console.warn(data);
            }
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно выполнить языки');
            console.error(e);
        }
    });
}
function on_search() {
    var button = $(this);
    if (button.hasClass("active") === false) {
        dom.table.on('search.bs.table', search);
        button.addClass('active btn-success');
    } else {
        dom.table.off('search.bs.table', search);
        button.removeClass('active').removeClass('btn-success');
    }
}
function remove_many() {
    var data = dom.table.bootstrapTable('getSelections');
    if (data.length === 0) {
        return;
    }
    var result = $.map(data, function (row) {
        return row.id;
    });
    $.ajax({
        url: cfg.url('user.remove_all', result),
        dataType: 'json',
        method: 'GET',
        success: function () {
            alert(dom.alert, 'alert-success', "успешно выполнено");
            refresh('remove', result);
            dom.modal.removeAll.modal('hide');
        },
        error: function (e) {
            alert(dom.alert, "alert-danger", 'не возможно выполнить языки');
            console.error(e);
            dom.modal.removeAll.modal('hide');
        }
    });
}
function user(data, _default) {
    var options = func.options(data, {name: 'login', value: 'id'}, _default);
    $('select.user-select').each(function (index, dom) {
        $(dom).html(options);
    });
}
function init() {
    dom = {
        modal: {
            edit: $(document.getElementById('modal-user-change')),
            remove: $(document.getElementById('modal-user-delete')),
            add: $(document.getElementById('modal-user-create')),
            removeAll: $(document.getElementById('modal-user-many-delete'))
        },
        table: $(document.getElementById('table-user')),
        user: $('select.user-select'),
//        table: $('#table-user'),
        alert: $(document.getElementById('alert-user')),
        form: {
            edit: $(document.getElementById('form-user-change')),
            remove: $(document.getElementById('form-user-delete')),
            add: $(document.getElementById('form-user-create'))
        },
        action: {
            panel: $(document.getElementById('panel-user-action')),
            add: $(document.getElementById('action-user-create')),
            edit: $(document.getElementById('action-user-change')),
            remove: {
                one: $(document.getElementById('action-user-delete')),
                many: $(document.getElementById('action-user-many-delete'))
            }
        }
    };
    configure = {
        method: 'GET',
        url: cfg.url('user.list'),
        toggle: "table",
        cache: false,
        striped: true,
        search: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, "ALL"],
        sidePagination: "client",
        ajaxOptions: {
            xhrFields: {withCredentials: true},
            crossDomain: true
        },
        queryParams: function (params) {
            return {in: params};
        },
        toolbar: '#toolbar-user',
        undefinedText: '',
        silent: true,
        idField: 'code',
        sortName: 'code',
        sortOrder: 'asc',
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'id',
                title: 'ID',
                align: 'center',
                valign: 'middle',
                sortable: true
//                formatter: nameFormatter
            }, {
                field: 'login',
                title: 'Login',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'password',
                title: 'Password',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'operate',
                title: 'Действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    db.index[row.id] = row.login;
                    db.data[index] = row;
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        var modal = dom.modal.edit;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value);
                        }
                        action_index = index;
                    },
                    'click .remove': function (e, value, row, index) {
                        var modal = dom.modal.remove;
                        modal.modal('show');
                        for (var name in row) {
                            var value = row[name];
                            modal.find('[name="' + name + '"]').val(value).attr('disabled', true);
                        }
                        action_index = index;
                    }
                }
            }]
    };
    dom.table.bootstrapTable(configure).on('pre-body.bs.table', function (e, data) {
        user(data, user.uid);
    });
    dom.action.add.click(add);
    dom.action.edit.click(edit);
    dom.action.remove.one.click(remove);
    dom.action.remove.many.click(remove_many);
}
module.exports = {
    init: init,
    cfg: configure,
    dom: dom,
    db: db
};