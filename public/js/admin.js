(function() {
  $(document.getElementById('action-genre-add')).click(function() {
    var content, name, value, values;
    content = $(document.getElementById('modal-genre'));
    values = {
      'id': '',
      'genre': '',
      'lang': 'ru'
    };
    for (name in values) {
      value = values[name];
      content.find('[name="' + name + '"]').val(value);
    }
  });

  $('#panel-genre-action').click(function() {
    $('#panel-genre-view-table').bootstrapTable({
      method: 'get',
      url: '/data/genres.json',
      toggle: 'table',
      cache: false,
      striped: true,
      pagination: true,
      pageSize: 50,
      pageList: [10, 25, 50, 100, 200],
      search: true,
      showColumns: true,
      showRefresh: true,
      showToggle: true,
      minimumCountColumns: 2,
      clickToSelect: true,
      columns: [
        {
          field: 'state',
          checkbox: true
        }, {
          field: 'id',
          title: 'ID',
          align: 'right',
          valign: 'bottom',
          sortable: true
        }, {
          field: 'genre',
          title: 'Жанр',
          align: 'center',
          valign: 'middle',
          sortable: true
        }, {
          field: 'lang',
          title: 'Язык',
          align: 'left',
          valign: 'top',
          sortable: true
        }, {
          field: 'operate',
          title: 'действия',
          align: 'center',
          valign: 'middle',
          clickToSelect: false,
          formatter: function(value, row, index) {
            return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' + '<i class="glyphicon glyphicon-edit"></i>' + '</a>' + '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' + '<i class="glyphicon glyphicon-remove"></i>' + '</a>';
          },
          events: {
            'click .edit': function(e, value, row, index) {
              var value;
              var content, name;
              $(document.getElementById('modal-genre')).modal('show');
              content = $(document.getElementById('modal-genre'));
              for (name in row) {
                value = row[name];
                content.find('[name="' + name + '"]').val(value);
              }
            },
            'click .remove': function(e, value, row, index) {
              var msg;
              msg = $(document.getElementById('alert-genre'));
              $.ajax({
                url: '/action=manga.action.genre.delete',
                dataType: 'json',
                method: 'GET',
                data: {
                  params: [row.id]
                },
                success: function(data) {
                  if (data.error !== void 0) {
                    alert(msg, 'alert-success', 'успешно выполнено');
                  } else {
                    alert(msg, 'alert-warning', data.error);
                  }
                },
                error: function(e) {
                  alert(msg, 'alert-danger', 'не возможно выполнить');
                }
              });
              console.log(value, row, index);
            }
          }
        }
      ]
    });
  });

}).call(this);
