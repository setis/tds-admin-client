$(document.getElementById('action-genre-add')).click(function () {
    var content = $(document.getElementById('modal-genre'));
    var values = {'id': "", 'genre': "", 'lang': "ru"};
    for (var name in values) {
        var value = values[name];
        content.find('[name="' + name + '"]').val(value);
    }
});
(function ($, window, undefined) {
    //is onprogress supported by browser?
    var hasOnProgress = ("onprogress" in $.ajaxSettings.xhr());

    //If not supported, do nothing
    if (!hasOnProgress) {
        return;
    }

    //patch ajax settings to call a progress callback
    var oldXHR = $.ajaxSettings.xhr;
    $.ajaxSettings.xhr = function () {
        var xhr = oldXHR();
        if (xhr instanceof window.XMLHttpRequest) {
            xhr.addEventListener('progress', this.progress, false);
        }

        if (xhr.upload) {
            xhr.upload.addEventListener('progress', this.progress, false);
        }

        return xhr;
    };
})(jQuery, window);
var loading = document.getElementById('loading');
$(document).on({
    ajaxStart: function () {
        console.log('ajaxStart');
        $(loading).show();
        loading.style.width = '100%';
    },
    ajaxStop: function () {
        console.log('ajaxStop');
        $(loading).hide();
    },
    progress: function (e) {
        if (e.lengthComputable) {
            var pct = (e.loaded / e.total) * 100;
            loading.style.width = pct + '%';
            console.log('ajaxProgress:' + pct + '%');
        }
    }

});
function alert(id,type,message){
    id.html('<div class="alert '+type+'"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><div>'+message+'</div></div>');
    id.find('.alert').alert();
}

$('#panel-genre-action').click(function () {
    $('#panel-genre-view-table').bootstrapTable({
        method: 'get',
        url: '/data/genres.json',
        toggle: "table",
        cache: false,
        striped: true,
        pagination: true,
        pageSize: 50,
        pageList: [10, 25, 50, 100, 200],
        search: true,
        showColumns: true,
        showRefresh: true,
        showToggle: true,
        minimumCountColumns: 2,
        clickToSelect: true,
//    selectItemName: "toolbar-genre",
        columns: [
            {
                field: 'state',
                checkbox: true
            },
            {
                field: 'id',
                title: 'ID',
                align: 'right',
                valign: 'bottom',
                sortable: true
            }, {
                field: 'genre',
                title: 'Жанр',
                align: 'center',
                valign: 'middle',
                sortable: true
//                formatter: nameFormatter
            }, {
                field: 'lang',
                title: 'Язык',
                align: 'left',
                valign: 'top',
                sortable: true
//                formatter: priceFormatter,
//                sorter: priceSorter
            }, {
                field: 'operate',
                title: 'действия',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: function (value, row, index) {
                    return '<a class="edit ml10" href="javascript:void(0)" title="Редактировать">' +
                            '<i class="glyphicon glyphicon-edit"></i>' +
                            '</a>' +
                            '<a class="remove ml10" href="javascript:void(0)" title="Удалить">' +
                            '<i class="glyphicon glyphicon-remove"></i>' +
                            '</a>';
                },
                events: {
                    'click .edit': function (e, value, row, index) {
                        $(document.getElementById('modal-genre')).modal('show');
                        var content = $(document.getElementById('modal-genre'));
                        for (var name in row) {
                            var value = row[name];
                            content.find('[name="' + name + '"]').val(value);
                        }
                    },
                    'click .remove': function (e, value, row, index) {
                        var msg = $(document.getElementById('alert-genre'));
                        $.ajax({
                            url: '/action=manga.action.genre.delete',
                            dataType: 'json',
                            method: 'GET',
                            data: {params: [row.id]},
                            success: function (data) {
                                if (data.error !== undefined) {
                                    alert(msg,'alert-success',"успешно выполнено");
                                } else {
                                    alert(msg,'alert-warning',data.error);
                                }
                            },
                            error: function (e) {
                                alert(msg,"alert-danger",'не возможно выполнить');
                            }
                        });
                        console.log(value, row, index);
                    }
                }
            }]
    });
});
 