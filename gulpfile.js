var gulp = require('gulp'),
        jquery = require('gulp-jquery'),
        lmd = require('gulp-lmd'),
        rename = require('gulp-rename'),
        jade = require('gulp-jade'),
        connect = require('gulp-connect'),
        coffee = require('gulp-coffee');
gulp.task('jquery', function () {
    // https://www.npmjs.com/package/jquery#fires-a-native-dom-event-without-going-through-jquery
    //https://www.npmjs.com/package/gulp-jquery
    return jquery.src({
        release: 2, //jQuery 2 
        flags: ['-ajax', '-ajax/xhr', '-event', '-exports/global']
    })
    .pipe(gulp.dest('./dist/'));
    // creates ./public/vendor/jquery.custom.js 
});
gulp.task('lmd', function () {
    return gulp.src('')
            .pipe(lmd('test/app.lmd.js'))
            .pipe(rename('app.js'))
            .pipe(gulp.dest('public/build'));
});
gulp.task('connect', function () {
    connect.server({
        root: './public',
        port: 8082,
        livereload: true
    });
});

gulp.task('reload', function () {
  gulp.src('public/*.html').pipe(connect.reload());
  gulp.src('public/css/*.css').pipe(connect.reload());
  gulp.src('public/js/*.js').pipe(connect.reload());
  gulp.src('public/img/*').pipe(connect.reload());
  gulp.src('public/fonts/*').pipe(connect.reload());
  gulp.src('public/fonts/*').pipe(connect.reload());
});
gulp.task('jade', function() {
  gulp.src('./jade/index.jade')
    .pipe(jade())
    .pipe(gulp.dest('./public/'));
});
gulp.task('coffee', function() {
  gulp.src('./coffee/*.coffee')
    .pipe(coffee())
    .pipe(gulp.dest('./public/js/'));
});
gulp.task('watch', function () {
    gulp.watch(['./public/css/*.css','./public/*.html','./public/js/*.js'], ['reload']);
    gulp.watch(['./jade/*.jade','./jade/*/*.jade'], ['jade']);
    gulp.watch(['./coffee/*.coffee'], ['coffee']);
    gulp.watch(['./jade/*/*.html'], ['jade']);
});
gulp.task('copy-js', function(){
  gulp.src('./js/*.js').pipe(gulp.dest('./public/js/',{overwrite: true}));
});
gulp.task('default', ['jade','connect','watch']);